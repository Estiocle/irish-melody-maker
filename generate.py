from TotalSheet import TotalSheet
import pysynth
import random

def aleaName():
	cara = ['q', 'w','e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'y', 'x', 'c', 'v', 'b', 'n', 'm']
	name = ''
	i = 0
	while i < 10:
		name += random.choice(cara)
		i += 1

	return name

styles = {'reel' : (4, 4.0), 'jig' : (6, 8.0), 'waltz' : (3, 4.0), 'slip jig' : (9, 8.0), 'hornpipe' : (4, 4.0), 'mazurka' : (3, 4.0), 'three-two' : (6, 4.0), 'polka' : (4, 4.0), 'barndance' : (4, 4.0), 'slide' : (12, 8.0), 'strathspey' : (4, 4.0)}
modes = ('major', 'minor', 'mixolydian', 'dorian')
choice = ''
choice2 = ''

for key in styles:
	choice += key + ', '
choice = choice[:-2]

for mode in modes:
	choice2 += mode + ', '
choice2 = choice2[:-2]

kind = ''
mode = ''

version = raw_input('choose a version of the algorithm (1 or 2) : \n')

while not kind in styles:
	kind = raw_input('select one of following type of tune : \n' + choice + '\n')

	while not mode in modes:
		mode = raw_input('select one of following mode : \n' + choice2 + '\n')
		if kind in styles and mode in modes:
			form = list(raw_input('give a form (ex : aaba or abca) : \n'))
			sheet = TotalSheet(form, version)
			sheet.createSong(kind, mode, styles[kind][0], styles[kind][1])
			formatSong = sheet.sheet

			for note in formatSong:
				note[1] = 2.0 / note[1]

			print formatSong

			pysynth.make_wav(formatSong, 200, 0, .15, 1.1, 0, 'outputs/' + kind + '_' + mode + '_' + aleaName())