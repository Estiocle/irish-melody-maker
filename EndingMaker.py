# cree des sons a partir du fichier de chaine de markov simple contenant tout styles et toutes tonalites
# utilise une chaine de Markov de degres 3 ou 2 pour la melodie
# cree un pattern rythmic et une melodie Modifie ce pattern selon un shema pseudo aleatoire
# les stats sont juste composee des 12 notes les octaves seront exposee dans l'algorithme
# utilise des chaine de markov de degre 3
import csv
import random
import json
import re

class EndingMaker:

	def __init__(self, style = 'reel', mode = 'major', numerator = 4, denominateur = 4.0):

		statsNoteEndingFile = open("sources/noteEnding.json")
		statsRythmEndingFile = open("sources/rythmEnding.json")

		self.statsNoteEnding = json.load(statsNoteEndingFile)
		self.statsRythmEnding = json.load(statsRythmEndingFile)

		self.modes = ('minor', 'mixolydian', 'dorian', 'major')
		self.keyboard = ('c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#', 'a', 'a#', 'b')
		self.completeKeyboard = ('c3', 'c#3', 'd3', 'd#3', 'e3', 'f3', 'f#3', 'g3', 'g#3', 'a3', 'a#3', 'b3', 'c4', 'c#4', 'd4', 'd#4', 'e4', 'f4', 'f#4', 'g4', 'g#4', 'a4', 'a#4', 'b4', 'c5', 'c#5', 'd5', 'd#5', 'e5', 'f5', 'f#5', 'g5', 'g#5', 'a5', 'a#5', 'b5', 'c6')
		self.rythms = ('1.0', '2.0', '3.0', '4.0', '6.0', '8.0', '12.0', '16.0')
		self.styles = ('jig', 'waltz', 'three-two', 'slip jig', 'hornpipe', 'polka', 'barndance', 'slide', 'strathspey', 'mazurka', 'reel')
		
		self.style = style
		self.mode = mode
		self.numerator = numerator
		self.denominateur = denominateur

	def setEnding(self, sheet):
		if sheet[-1] == 't':
			nbr = random.choice([1, 2])

			if nbr == 1:
				sheet = sheet[0:-2]
				# sheet[-1][1] = (self.denominateur / 4)
		elif sheet[-2] != 't':
			endingsRythm = []

			for rythm1 in self.rythms:
				for rythm2 in self.rythms:
					if self.statsRythmEnding.has_key(self.style + rythm1 + rythm2):
						nbrEndings = self.statsRythmEnding[self.style + rythm1 + rythm2]
						i = 0
						while i < nbrEndings:
							endingsRythm.append([(float(rythm1) / 4), (float(rythm2) / 4)])
							i = i + 1
		endingRythm = random.choice(endingsRythm)

		endingDuration = sum(endingRythm)

		i = -1
		totalDeleted = 0
		while totalDeleted < endingDuration:
			totalDeleted += sheet[i][1]
			i = i - 1

		supNote = sheet[i][0]
		sheet = sheet[:i]

		if totalDeleted > endingDuration:
			sheet.append([supNote, totalDeleted - endingDuration])

		sheet.append([supNote, endingRythm[0]])
		sheet.append([supNote, endingRythm[1]])

		endingsNote = []
		for note1 in self.keyboard:
			for note2 in self.keyboard:
				if self.statsNoteEnding.has_key(self.style + self.mode + sheet[-3][0][:-1] + note1 + note2):
					nbr = self.statsNoteEnding[self.style + self.mode + sheet[-3][0][:-1] + note1 + note2]
					i = 0
					while i < nbr:
						endingsNote.append([note1, note2])
						i = i + 1
		endingNote = random.choice(endingsNote)

		sheet[-1][0] = endingNote[1] + sheet[-1][0][-1]
		sheet[-2][0] = endingNote[0] + sheet[-2][0][-1]

		return sheet