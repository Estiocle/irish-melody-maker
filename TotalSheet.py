from SheetPart_v2 import SheetPart_v2
from SheetPart_v1 import SheetPart_v1
from EndingMaker import EndingMaker
import copy

class TotalSheet:
	
	def __init__(self, form = ['a', 'b', 'a'], version = 2):
		
		self.form = form
		self.sheet = []
		self.version = version

	def getPart(self, style = 'reel', mode = 'major', numerator = 4, denominator = 4.0):

		if self.version > 1:
			part = SheetPart_v2(style, mode, numerator, denominator)
		else:
			part = SheetPart_v1(style, mode, numerator, denominator)

		part.setSheet()
		songPart = []

		for note in part.melodicSheet:
			songPart.append([note, 0])

		rythmList = sum(part.rythmicSheet, [])

		i = 0
		for rythm in rythmList:
			songPart[i][1] = rythm
			i += 1

		return songPart

	def createSong(self, style = 'reel', mode = 'major', numerator = 4, denominator = 4.0):

		parts = dict()

		for part in self.form:

			if not parts.has_key(part) : 
				parts[part] = self.getPart(style, mode, numerator, denominator)

		for part in self.form:
			for note in parts[part]:
				self.sheet.append(copy.copy(note))

		self.setEnding(style, mode, numerator, denominator)

	def setEnding(self, style, mode, numerator, denominator):
		ending = EndingMaker(style, mode, numerator, denominator)
		self.sheet = ending.setEnding(self.sheet)