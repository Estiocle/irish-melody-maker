"""
Parse a file in ABC music notation format to be interpret with pysynth.
Based on pysynth library
used and modified By Noe Zufferey - Mars 2015
"""

import json
import csv
import sys, urllib2
import pysynth

sel = False
isAsong = True
num = 1
song = []



# flatten or sharpen notes according to key signature
# key_sig is in range [-7 .. + 7] meaning that many
# flats (-ve) or sharps (+ve)

key_sigs = (
  ("C",    "Am",    0),
  ("GMix", "DDor",  0),
  ("G",    "Em",    1),
  ("DMix", "ADor",  1),
  ("F",    "Dm",   -1),
  ("CMix", "GDor", -1),
  ("D",    "Bm",    2),
  ("AMix", "EDor",  2),
  ("HP",   "Hp",    2),         # Highland pipes dontcha know
  ("Bb",   "Gm",   -2),
  ("FMix", "CDor", -2),
  ("A",    "F#m",   3),
  ("EMix", "BDor",  3),
  ("Eb",   "Cm",   -3),         # Suspect that more than this will never be
  ("BbMix","FDor", -3),         # used, but what the hell ...
  ("E",    "C#m",   4),
  ("BMix", "F#Dor", 4),
  ("Ab",   "Fm",   -4),
  ("EbMix","BbDor",-4),
  ("B",    "G#m",   5),
  ("F#Mix","C#Dor", 5),
  ("Db",   "Bbm",  -5),
  ("AbMix","EbDor",-5),
  ("F#",   "D#m",   6),
  ("C#Mix","G#Dor", 6),
  ("Gb",   "Ebm",  -6),
  ("DbMix","AbDor",-6),
  ("C#",   "A#m",   7),
  ("G#Mix","D#Dor", 7),
  ("Cb",   "Abm",  -7),
  ("GbMix","DbDor",-7),
)

# A table of which note is flattened or sharpened next.
# ( 1st flat = B, 2nd = E, 3rd = A ...
#   1st sharp = F, second = C, 3rd = G ...)

alphabet = ('q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'y', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Z', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Y', 'X', 'C', 'V', 'B', 'N', 'M')
notesABC = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'z')
flats_and_sharps = '', 'f', 'c', 'g', 'd', 'a', 'e', 'b'

keys_s = ('a', 'a#', 'b', 'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#')
keys_f = ('a', 'bb', 'b', 'c', 'db', 'd', 'eb', 'e', 'f', 'gb', 'g', 'ab')

modes = ('minor', 'mixolydian', 'dorian', 'major')

keyboard = ('c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#', 'a', 'a#', 'b', 'r')
rythms = ('1.0', '2.0', '3.0', '4.0', '6.0', '8.0', '12.0', '16.0', 't')
styles = ('jig', 'waltz', 'three-two', 'slip jig', 'hornpipe', 'polka', 'barndance', 'slide', 'strathspey', 'mazurka', 'reel')

piano_s, piano_f = [], []

for k in range(88):
	oct = (k + 9) // 12
	note = '%s%u' % (keys_s[k%12], oct)
	piano_s.append(note)
	note = '%s%u' % (keys_f[k%12], oct)
	piano_f.append(note)

def simp_line(a):
	a2, a3 = [], []
	ign = False
	ghost = False
	i = 0
	for x in a:
		if x == '"':
			i += 1
	if not i == 1:
		for x in a:
			if x == '"': ign = not ign
			if x == '{': ghost = True
			if x == '}': ghost = False
			if not (ign or ghost):
				a2 += x
		for x in range(len(a2)):
			if a2[x] == '[':
				c = x
				found_col = False
				for x2 in range(c, len(a2)):
					if a2[x2] == ':':
						found_col = True
					if a2[x2] == ']' and found_col:
						for x3 in range(c, x2 + 1):
							a2[x3] = ' '
						break
	return ''.join(a2)
#
chord = False
tie_next = 0
second_ver, do_repeat, only_first, triplet = [], False, False, 0

def add_note(a, n):
	global song, chord, measure_sharps_flats, tie_next, second_ver, do_repeat, only_first, triplet, tripfac

	start, note, leng, next_half, firstnote = (False, None, float(unit), False, '')
	for x in range(n, len(a)):
		if note and a[x] in (' ', '>', '(', '|', ':'):
			break
		if a[x] == '>' and len(song) != 0:
			l = 1. / song[-1][1]
			l = 1.5 * l
			song[-1][1] = 1. / l
			next_half = True
			continue
		if not note and a[x] == '%':
			return
		if not note and a[x] == '(' and a[x + 1].isdigit() and a[x + 2] in notesABC:
			triplet = int(a[x + 1])
			tripfac = triptab[triplet]
			try:
				if a[x + 2] == ':':
					tripfac = float(triplet) / float(a[x+3])
					if a[x + 4] == ':':
						triplet = int(a[x + 5])
			except: pass
		if not note and a[x] == '[':
			i = 1
			nbrNotes = 0
			while not (x + i < len(a) - 1) and (not (a[x + i] == ']')):
				if nbrNotes >= 2:
					chord = True
					break
				if a[x + i] in notesABC:
					nbrNotes += 1
				i += 1
			continue
		if not note and a[x] == '|':
			firstnote = '*'
			measure_sharps_flats = global_sharps_flats.copy()
			if x < len(a) - 1:
				if a[x + 1] == ':':
					second_ver, do_repeat = [], True
				if a[x + 1] == '1':
					only_first = True
				if a[x + 1] == '2':
					only_first = False
			continue
		if not note and a[x] == ':':
			if x < len(a) - 1:
				if a[x + 1] == ':' or a[x + 1] == '|':
					song = song + second_ver
					second_ver, do_repeat = [], False
		if note and a[x] == '-':
			tie_next = 2
			continue
		if a[x] == ',' and a[x - 1] in notesABC:
			oct -= 1
			note_oct = "%s%u" % (note, oct)
			continue
		if a[x] == '\'' and a[x - 1] in notesABC:
			oct += 1
			note_oct = "%s%u" % (note, oct)
			continue
		if not note and a[x].isalpha():
			note = a[x].lower()
			if a[x].isupper(): oct = 4
			else: oct = 5
			note_oct = "%s%u" % (note, oct)
			if a[x - 1] == '_':
				for oct2 in range(9):
					note_oct2 = "%s%u" % (note, oct2)
					orig = measure_sharps_flats.get(note_oct2, 0)
					measure_sharps_flats[note_oct2] = orig - 1
			if a[x - 1] == '^':
				for oct2 in range(9):
					note_oct2 = "%s%u" % (note, oct2)
					orig = measure_sharps_flats.get(note_oct2, 0)
					measure_sharps_flats[note_oct2] = orig + 1
			if a[x - 1] == '=':
				for oct2 in range(9):
					note_oct2 = "%s%u" % (note, oct2)
					measure_sharps_flats[note_oct2] = 0
			continue
		if note and a[x].isdigit():
			leng = float(unit) / float(a[x])
			continue
		if note and a[x] == '/':
			try: fac = float(a[x - 1]) / float(a[x + 1])
			except: fac = .5
			leng = float(unit) / fac
		if note and a[x].isalpha() or a[x] == '[':
			break

	if note:
		if triplet:
			leng *= tripfac
			triplet -= 1
		if note[0].lower() == 'z':
			note = 'r'
			song += [["%s" % note, leng]]
			if not only_first:
				second_ver += [["%s" % note, leng]]
		else:
			corr_note = piano[piano.index(note_oct) + measure_sharps_flats.get(note_oct, 0)]
			corr_note = "%s%s" % (corr_note, firstnote)
			if tie_next == 1:
				if len(song) != 0 and corr_note == song[-1][0]:
					song[-1][1] = 1. / (1./song[-1][1] + 1. / leng)
					try:
						second_ver[-1][1] = song[-1][1]
					except: pass
				tie_next = 0
			else:
				song += [[corr_note, leng]]
				if not only_first:
					second_ver += [[corr_note, leng]]
				if next_half:
					leng = 1. / song[-1][1]
					song[-1][1] = 1. / (.5 * leng)
					next_half = False
				if tie_next == 2: tie_next = 1
		return x
	else: 
		return 0
#
def parse_line(a):
	global chord
	n = 0
	while n < len(a):
		n = add_note(a, n)
		if chord:
			for n2 in range(n, len(a)):
				if a[n2] == ']':
					n = n2
					chord = False
					break
		if not n: break
#
def mk_triptab(m):
	if int(m.split('/')[0]) % 2:
		n = 3
	else:
		n = 2
	return {2: 2./3., 3: 1.5, 4: 4./3., 5: 5./n, 6: 3., 7: 7./n, 8: 8./3., 9: 9./n}
#
def get_bpm(s, u = "1/4"):
	if '=' not in s:
		c, d = u.split('/')
		return int(s) * 4. * float(c) / float(d)
	else:
		a, b = s.split('=')
		c, d = a.split('/')
		return int(b) * 4. * float(c) / float(d)

def normalize(song, mode):
	tonic = mode[0]
	tonics = ('C', 'D', 'E', 'F', 'G', 'A', 'B')
	notes = ('c', 'd', 'e', 'f', 'g', 'a', 'b')
	notesPosition = {'c': 0, 'd': 1, 'e': 2, 'f': 3, 'g': 4, 'a': 5, 'b': 6}
	tones = {'C': 0, 'D': 1, 'E': 2, 'F': 3, 'G': 4, 'A': 5, 'B': 6}
	minor = -5
	mixo = -4
	dorian = -1

	if mode[1:] == 'minor':
		tonic = tonics[tones[tonic] + minor]
	if mode[1:] == 'mixolydian':
		tonic = tonics[tones[tonic] + mixo]
	if mode[1:] == 'dorian':
		tonic = tonics[tones[tonic] + dorian]

	for n in song:
		if not (n[0][0] == 'r' or n[0][0] == 'x'):
			if n[0][-1] == '*':
				n[0] = n[0][0:-1]
			if n[0][-1] in ['1', '2', '3', '4', '5', '6', '7']:
				n[0] = n[0][0:-1]
			if not mode == 'Cmajor':
				n[0] = n[0].replace(n[0][0], notes[notesPosition[n[0][0]] - tones[tonic]])

	return song
#
	
sources = open('sources/TheSession-data/csv/tunes.csv', 'r')
measureStats = dict()
repetitionStats = dict()
typeStats = dict()
keyStats = dict()
nbrMeasure = 0
nbrTunes = 0
readerSources = csv.reader(sources)
i = 0
for data in readerSources:
	sel = False
	isAsong = True
	num = 1
	song = []
	if i <= 20000:
		isAsong = True
		isNotText = True
		for l in data[6]:
			if l == '\\\'':
				isNotText = not isNotText
			if isNotText:
				if l in alphabet and not l in notesABC:
					isAsong = False

		abc = "X: 1 \nM: {0} \nK: {1} \n{2}{3}".format(data[4], data[5], data[6].replace('\\\'', '"').replace('[1', '1').replace('[2', '2').replace('|,', '|'), '|')
		bpm     = 120
		meter   = "4/4"
		triptab = None
		nunit   = "1/4"
		unit    = 4
		numberMetric = 0
		abc = abc.split('\n')

		if not measureStats.has_key(data[3]):
			measureStats[data[3]] = 0

		if not repetitionStats.has_key(data[3]):
			repetitionStats[data[3]] = 0

		if not typeStats.has_key(data[3]):
			typeStats[data[3]] = 1
		else:
			typeStats[data[3]] += 1

		if not keyStats.has_key(data[3]):
			keyStats[data[3]] = dict()

		if not keyStats[data[3]].has_key(data[5]):
			keyStats[data[3]][data[5]] = 1
		else:
			keyStats[data[3]][data[5]] += 1

		nbrTunes += 1

		for l in abc:
			j = 1
			k = 0
			while j < len(l):
				if l[j] == '|':
					k += 1
					measureStats[data[3]] += 1
					nbrMeasure += 1
				if l[j] == '|' and l[j - 1] == ':':
					repetitionStats[data[3]] += 1
					measureStats[data[3]] += k
					k = 0
				j += 1
	i += 1

print 'repetitions'
for stat in repetitionStats:
	print stat + ' : ' + str(repetitionStats[stat] / typeStats[stat])
print
print 'nbr de mesure en moyenne'
print nbrMeasure / nbrTunes
print
print 'nombre de mesure'
for stat in measureStats:
	print stat + ' : ' + str(measureStats[stat] / typeStats[stat])
print
print 'clefs'
for stats in keyStats:
	for stat in keyStats[stats]:
		s = ''
		n = 0
		if keyStats[stats][stat] > n:
			s = stat
			n = keyStats[stats][stat]
		
	print stats + ' - ' + stat + ' : ' + str(keyStats[stats][stat])