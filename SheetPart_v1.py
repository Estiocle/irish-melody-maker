# cree des sons a partir du fichier de chaine de markov simple contenant tout styles et toutes tonalites
# utilise une chaine de Markov de degres 3 ou 2 pour la melodie
# cree un pattern rythmic et une melodie Modifie ce pattern selon un shema pseudo aleatoire
# les stats sont juste composee des 12 notes les octaves seront exposee dans l'algorithme
# utilise des chaine de markov de degre 3
import csv
import random
import json
import re

class SheetPart_v1:

	def __init__(self, style = 'reel', mode = 'major', numerator = 4, denominateur = 4.0):
		statsMelFile = open("sources/markovMel3.json")

		statsRythmFile = open("sources/rythm.json")
		statsNoteRythmFile = open("sources/noteRythm.json")

		statsNoteRythm = json.load(statsNoteRythmFile)
		statsRythm = json.load(statsRythmFile)
		statsMel = json.load(statsMelFile)

		self.modes = ('minor', 'mixolydian', 'dorian', 'major')
		self.keyboard = ('c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#', 'a', 'a#', 'b')
		self.completeKeyboard = ('c3', 'c#3', 'd3', 'd#3', 'e3', 'f3', 'f#3', 'g3', 'g#3', 'a3', 'a#3', 'b3', 'c4', 'c#4', 'd4', 'd#4', 'e4', 'f4', 'f#4', 'g4', 'g#4', 'a4', 'a#4', 'b4', 'c5', 'c#5', 'd5', 'd#5', 'e5', 'f5', 'f#5', 'g5', 'g#5', 'a5', 'a#5', 'b5', 'c6')
		self.rythms = ('1.0', '2.0', '3.0', '4.0', '6.0', '8.0', '12.0', '16.0', 't')
		self.styles = ('jig', 'waltz', 'three-two', 'slip jig', 'hornpipe', 'polka', 'barndance', 'slide', 'strathspey', 'mazurka', 'reel')
		
		self.sheet = []
		self.rythmicSheet = []
		self.melodicSheet = []
		self.style = style
		self.mode = mode
		self.numerator = numerator
		self.denominateur = denominateur
		self.duration = []
		self.melodicMarkov = statsMel

		totalRythmData = 0.0
		rythmData = dict()
		for rythm in self.rythms:
			totalRythmData += statsRythm[self.style + rythm]
			rythmData[rythm] = statsRythm[self.style + rythm]

		for rythm in self.rythms:
			rythmData[rythm] = int(round((rythmData[rythm] / totalRythmData) * 10000))

		for rythm in self.rythms:
			i = 0
			while i < rythmData[rythm]:
				if rythm == 't':
					self.duration.append(1.0/3)
				else:
					self.duration.append(float(rythm) / 4)
				i += 1

		self.duration = map(lambda x : x * (self.denominateur / 4), self.duration)

	def getStatNoteTest(self):
		for stat in self.melodicMarkov:
			if self.melodicMarkov[stat] > 0:
				print stat
				print self.melodicMarkov[stat]
				print

	def getStatNote(self):
		expr = '^' + self.mode + '(.)*'
		stats = []

		for note in self.keyboard:
			stats[note] = 0
			tempExpr = re.compile(expr + note)
			for key in self.melodicMarkov:
				if tempExpr.match(key):
					stats[note] += self.melodicMarkov[key]
		return stats

	def getMeasure(self):
		measure = []
		while sum(measure) < self.numerator:
			choice = random.choice(self.duration)
			value = choice
			if choice == 1.0/3:
				if (sum(measure) - round(sum(measure))) != 0:
					continue
			if choice == 3.0/4:
				if (sum(measure) - round(sum(measure))) != 0:
					continue
			if choice == 3.0/2:
				if (sum(measure) - round(sum(measure))) != 0:
					continue
				value = 1
			if choice == 1.0/4:
				value = 1.0/2
			if choice == 3.0/4:
				value = 1.0
			if choice == 3.0/2:
				value = 2.0
			if sum(measure) + value <= self.numerator:
				measure.append(choice)
				if choice == 1.0/3:
					measure.append(choice)
					measure.append(choice)
				if choice == 1.0/4:
					measure.append(choice)
				if choice == 3.0/4:
					measure.append(1.0/4)
				if choice == 3.0/2:
					alea = random.choice((0, 1))
					if alea:
						measure.append(1.0/2)
					else:
						measure.append(1.0/4)
						measure.append(1.0/4)
		return measure

	def	setRythmicPart(self, nbrMeasure = 4):
		patternLenght = random.choice([2, 4])
		print patternLenght
		part = []
		j = 0
		while j < nbrMeasure:
			if j < patternLenght:
				part.append(self.getMeasure())
			# elif j == nbrMeasure - 1:
			# 	part.append(self.getMeasure())
			else:
				part.append(part[j - patternLenght])
			j += 1
		self.rythmicSheet = part
		return patternLenght

	def getNote(self, lastNote, lastNoteBefore, lastNoteBeforeBefore = ''):
		# choix aleatoire selon les sats
		if lastNote != 'x':
			lastNote = lastNote[0:-1]
		if lastNoteBefore != 'x':
			lastNoteBefore = lastNoteBefore[0:-1]
		if lastNoteBeforeBefore != 'x' and lastNoteBeforeBefore != '':
			lastNoteBeforeBefore = lastNoteBeforeBefore[0:-1]


		nbrNoteTotal = 0.0
		notes = []

		for note in self.keyboard:
			if self.melodicMarkov.has_key(self.style + self.mode + lastNoteBeforeBefore + lastNoteBefore + lastNote + note):
				nbrNoteTotal += self.melodicMarkov[self.style + self.mode + lastNoteBeforeBefore + lastNoteBefore + lastNote + note]

		for note in self.keyboard:
			if self.melodicMarkov.has_key(self.style + self.mode + lastNoteBeforeBefore + lastNoteBefore + lastNote + note):
				i = 0
				while i < ((self.melodicMarkov[self.style + self.mode + lastNoteBeforeBefore + lastNoteBefore + lastNote + note] / nbrNoteTotal) * 1000):
					notes.append(note)
					i += 1
		return random.choice(notes)

	def setMelodicSheet(self, patternLenght):
		# mettre une note sur chaque valeur rythmique
		j = 0
		i = 0
		for measure in self.rythmicSheet:
			if j < patternLenght:
				if j % patternLenght == 0 or random.choice([True, False, False, False]):
					lastNote = 'x'
					lastNoteBefore = 'x'
					lastNoteBeforeBefore = 'x'

				for rythm in measure:
					choice = self.getNote(lastNote, lastNoteBefore, lastNoteBeforeBefore)
					choice = choice + '4'
					self.melodicSheet.append(choice)
					lastNoteBeforeBefore = lastNoteBefore
					lastNoteBefore = lastNote
					lastNote = choice
			else:
				for rythm in measure:
					self.melodicSheet.append(self.melodicSheet[i])
					i += 1
			j += 1

	def downNotesMelodicSheet(self):
		i = 0
		while i < len(self.melodicSheet) - 1:

			if self.completeKeyboard.index(self.melodicSheet[i + 1]) - self.completeKeyboard.index(self.melodicSheet[i]) >= 9:
				self.melodicSheet[i + 1] = self.melodicSheet[i + 1][0:-1] + str(int(self.melodicSheet[i + 1][-1]) - 1)
			
			i += 1

	def upNotesMelodicSheet(self):
		i = 0
		while i < len(self.melodicSheet) - 1:

			if  self.completeKeyboard.index(self.melodicSheet[i]) - self.completeKeyboard.index(self.melodicSheet[i + 1]) >= 9:
				self.melodicSheet[i + 1] = self.melodicSheet[i + 1][0:-1] + str(int(self.melodicSheet[i + 1][-1]) + 1)
			
			i += 1

	def setSheet(self, form = []):
		patternLenght = self.setRythmicPart()
		self.setMelodicSheet(patternLenght)
		self.downNotesMelodicSheet()
		self.upNotesMelodicSheet()