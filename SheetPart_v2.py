# cree des sons a partir du fichier de chaine de markov simple contenant tout styles et toutes tonalites
# utilise une chaine de Markov de degres 3 ou 2 pour la melodie
# cree un pattern rythmic et une melodie Modifie ce pattern selon un shema pseudo aleatoire
# les stats sont juste composee des 12 notes les octaves seront exposee dans l'algorithme
# utilise des chaine de markov de degre 3
import csv
import random
import json
import re

class SheetPart_v2:

	def __init__(self, style = 'reel', mode = 'major', numerator = 4, denominator = 4.0):

		statsMelFile = open("sources/markovMel3.json")

		statsRythmFile = open("sources/rythm.json")
		statsNoteRythmFile = open("sources/noteRythm.json")
		statsMarkovRythmFile = open("sources/markovRythm.json")

		statsNoteRythm = json.load(statsNoteRythmFile)
		statsRythm = json.load(statsRythmFile)
		statsMel = json.load(statsMelFile)
		statsMarkovRythm = json.load(statsMarkovRythmFile)

		self.modes = ('minor', 'mixolydian', 'dorian', 'major')
		self.keyboard = ('c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#', 'a', 'a#', 'b', 'r')
		self.completeKeyboard = ('c3', 'c#3', 'd3', 'd#3', 'e3', 'f3', 'f#3', 'g3', 'g#3', 'a3', 'a#3', 'b3', 'c4', 'c#4', 'd4', 'd#4', 'e4', 'f4', 'f#4', 'g4', 'g#4', 'a4', 'a#4', 'b4', 'c5', 'c#5', 'd5', 'd#5', 'e5', 'f5', 'f#5', 'g5', 'g#5', 'a5', 'a#5', 'b5', 'c6')
		self.rythmBoard = ('s1.0', 'd1.0', 'i1.0', 'q1.0', 's2.0', 'd2.0', 'i2.0', 'q2.0', 's3.0', 's4.0', 's6.0', 's8.0', 's12.0', 's16.0', 'it')
		self.rythms = ('1.0', '2.0', '3.0', '4.0', '6.0', '8.0', '12.0', '16.0', 't')
		self.styles = ('jig', 'waltz', 'three-two', 'slip jig', 'hornpipe', 'polka', 'barndance', 'slide', 'strathspey', 'mazurka', 'reel')
		
		self.sheet = []
		self.rythmicSheet = []
		self.melodicSheet = []
		self.style = style
		self.mode = mode
		self.numerator = numerator
		self.denominator = denominator
		self.duration = []
		self.melodicMarkov = statsMel
		self.rythmicMarkov = statsMarkovRythm

		totalRythmData = 0.0
		rythmData = dict()
		for rythm in self.rythms:
			totalRythmData += statsRythm[self.style + rythm]
			rythmData[rythm] = statsRythm[self.style + rythm]

		for rythm in self.rythms:
			rythmData[rythm] = int(round((rythmData[rythm] / totalRythmData) * 10000))

		for rythm in self.rythms:
			i = 0
			while i < rythmData[rythm]:
				if rythm == 't':
					self.duration.append(1.0/3)
				else:
					self.duration.append(float(rythm) / 4)
				i += 1

		self.duration = map(lambda x : x * (self.denominator / 4), self.duration)

	def getStatNoteTest(self):
		for stat in self.melodicMarkov:
			if self.melodicMarkov[stat] > 0:
				print stat
				print self.melodicMarkov[stat]
				print

	def getStatNote(self):
		expr = '^' + self.mode + '(.)*'
		stats = []

		for note in self.keyboard:
			stats[note] = 0
			tempExpr = re.compile(expr + note)
			for key in self.melodicMarkov:
				if tempExpr.match(key):
					stats[note] += self.melodicMarkov[key]
		return stats

	def getRythm(self, lastRythm, lastRythmBefore):
		nbrNoteTotal = 0.0
		rythms = []

		for rythm in self.rythmBoard:
			if self.rythmicMarkov.has_key(self.style + lastRythmBefore + lastRythm + rythm):
				nbrNoteTotal += self.rythmicMarkov[self.style + lastRythmBefore + lastRythm + rythm]

		for rythm in self.rythmBoard:
			if self.rythmicMarkov.has_key(self.style + lastRythmBefore + lastRythm + rythm):
				i = 0
				while i < ((self.rythmicMarkov[self.style + lastRythmBefore + lastRythm + rythm] / nbrNoteTotal) * 1000):
					rythms.append(rythm)
					i += 1
		return random.choice(rythms)
	
	def	setRythmicPart(self, nbrMeasure = 4):
		part = []
		rythmicPatternLengths = {'s' : 1, 'd' : 2, 'i' : 3, 'q' : 4}
		patternLenght = random.choice([2, 4])
		lastRythmBefore = 'x'
		lastRythm = 'x'
		timeLength = self.numerator * (4 /(self.denominator / 4))

		l = 0
		while l < patternLenght:
			k = 0
			measure = []
			while sum(measure) < timeLength and k < 1000:
				rythm = self.getRythm(lastRythm, lastRythmBefore)
				rythmicValue  = rythm[1:]

				if rythmicValue == 't':
					rythmicValue = 8.0 / 6
				if sum(measure) + (float(rythmicValue) * rythmicPatternLengths[rythm[0]]) <= timeLength:
					i = 0
					while i < rythmicPatternLengths[rythm[0]]:
						measure.append(float(rythmicValue))
						i = i + 1
					
					lastRythmBefore = lastRythm
					lastRythm = rythm
				k += 1
			part.append(measure)
			l += 1

		j = 0
		while j < nbrMeasure / patternLenght:
			part = part + part
			j += 1


		for measure in part:
			m = []
			for note in measure:
				if note == 't':
					m.append(1.0/3)
				else:
					m.append(note / 4)
			self.rythmicSheet.append(m)
		return patternLenght

	def getNote(self, lastNote, lastNoteBefore, lastNoteBeforeBefore = ''):
		# choix aleatoire selon les sats
		if lastNote != 'x':
			lastNote = lastNote[0:-1]
		if lastNoteBefore != 'x':
			lastNoteBefore = lastNoteBefore[0:-1]
		if lastNoteBeforeBefore != 'x' and lastNoteBeforeBefore != '':
			lastNoteBeforeBefore = lastNoteBeforeBefore[0:-1]


		nbrNoteTotal = 0.0
		notes = []

		for note in self.keyboard:
			if self.melodicMarkov.has_key(self.style + self.mode + lastNoteBeforeBefore + lastNoteBefore + lastNote + note):
				nbrNoteTotal += self.melodicMarkov[self.style + self.mode + lastNoteBeforeBefore + lastNoteBefore + lastNote + note]

		for note in self.keyboard:
			if self.melodicMarkov.has_key(self.style + self.mode + lastNoteBeforeBefore + lastNoteBefore + lastNote + note):
				i = 0
				while i < ((self.melodicMarkov[self.style + self.mode + lastNoteBeforeBefore + lastNoteBefore + lastNote + note] / nbrNoteTotal) * 1000):
					notes.append(note)
					i += 1
		return random.choice(notes)

	def setMelodicSheet(self, patternLenght):
		# mettre une note sur chaque valeur rythmique
		j = 0
		i = 0
		for measure in self.rythmicSheet:
			if j < patternLenght or random.choice([True, False, False, False]):
				if j % patternLenght == 0:
					lastNote = 'x'
					lastNoteBefore = 'x'
					lastNoteBeforeBefore = 'x'

				for rythm in measure:
					choice = self.getNote(lastNote, lastNoteBefore, lastNoteBeforeBefore)
					choice = choice + '4'
					self.melodicSheet.append(choice)
					lastNoteBeforeBefore = lastNoteBefore
					lastNoteBefore = lastNote
					lastNote = choice
			else:
				for rythm in measure:
					self.melodicSheet.append(self.melodicSheet[i])
					i += 1
			j += 1

	def downNotesMelodicSheet(self):
		i = 0
		while i < len(self.melodicSheet) - 1:

			if self.completeKeyboard.index(self.melodicSheet[i + 1]) - self.completeKeyboard.index(self.melodicSheet[i]) >= 9:
				self.melodicSheet[i + 1] = self.melodicSheet[i + 1][0:-1] + str(int(self.melodicSheet[i + 1][-1]) - 1)
			
			i += 1

	def upNotesMelodicSheet(self):
		i = 0
		while i < len(self.melodicSheet) - 1:

			if  self.completeKeyboard.index(self.melodicSheet[i]) - self.completeKeyboard.index(self.melodicSheet[i + 1]) >= 9:
				self.melodicSheet[i + 1] = self.melodicSheet[i + 1][0:-1] + str(int(self.melodicSheet[i + 1][-1]) + 1)
			
			i += 1

	def setSheet(self, form = []):
		patternLenght = self.setRythmicPart()
		self.setMelodicSheet(patternLenght)
		self.downNotesMelodicSheet()
		self.upNotesMelodicSheet()